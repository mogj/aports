# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-workspace-wallpapers
pkgver=6.1.2
pkgrel=0
pkgdesc="Wallpapers for the Plasma Workspace"
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-workspace-wallpapers.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-wallpapers-$pkgver.tar.xz"
# No tests available
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
48647c2644167a6ad54bc067456f1cd13336d514dfe53bef03d9d87d2b14d9efd08db950f396de699316bab5c8c1fcc604ac3f3e36cfb1d3fbc50003c3a0910b  plasma-workspace-wallpapers-6.1.2.tar.xz
"
