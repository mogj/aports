# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kgamma
pkgver=6.1.2
pkgrel=0
pkgdesc="Adjust your monitor's gamma settings"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	ki18n-dev
	qt6-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-doc $pkgname-lang"
install="$pkgname.post-install $pkgname.post-upgrade"
_repo_url="https://invent.kde.org/plasma/kgamma.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kgamma-$pkgver.tar.xz"

provides="kgamma5"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0d59963aa25ef57f6bafad4b8523610f599aaaeafaa97c467a114fb3dc06499efe4767e61e897b9eb4e76af7a079932d13778afc54907d0e147aa25d3fae58a4  kgamma-6.1.2.tar.xz
"
