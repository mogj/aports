# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma5support
pkgver=6.1.2
pkgrel=1
pkgdesc="Support components for porting from KF5/Qt5 to KF6/Qt6"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kpackage-dev
	kservice-dev
	kxmlgui-dev
	libksysguard-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qttools-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plasma5support.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma5support-$pkgver.tar.xz"

replaces="plasma-workspace<=6.0.5.1-r0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E pluginloadertest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

lang() {
	replaces="plasma-workspace-lang<=6.0.5.1-r0"

	default_lang
}
sha512sums="
2c05f8e39f1d10fcf1b353659b80fe724d141b5a241d72c940c64e71f1249bceb8590ae72932b543f6baa80ab64998e558cdc831d3302c12abf85c840f35126a  plasma5support-6.1.2.tar.xz
"
