# Contributor: qaqland <qaq@qaq.land>
# Maintainer: qaqland <qaq@qaq.land>
pkgname=rustlings
pkgver=6.0.1
pkgrel=0
pkgdesc="Small exercises to get you used to reading and writing Rust code"
url="https://rustlings.cool"
arch="all"
license="MIT"
depends="rust-clippy cargo"
makedepends="cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rust-lang/rustlings/archive/refs/tags/v$pkgver.tar.gz"
options="net" # cargo fetch

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
	cargo run -- dev check --require-solutions
}

package() {
	install -Dm755 target/release/rustlings "$pkgdir"/usr/bin/rustlings

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
	install -Dm644 README.md "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
6d083b833dd4290f61b9b1fcc3d6293a7952a3b1b5abda850fe7ef681d1e5524640ed414e86f5fa3c026449a6775825d94aa07ac02e2ac8a1c0714668c47bb52  rustlings-6.0.1.tar.gz
"
